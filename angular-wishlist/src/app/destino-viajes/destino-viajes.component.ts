import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-destino-viajes',
  templateUrl: './destino-viajes.component.html',
  styleUrls: ['./destino-viajes.component.css']
})
export class DestinoViajesComponent implements OnInit {
  @Input() nombre: string;
  constructor() { 
  }

  ngOnInit(): void {
  }

}
